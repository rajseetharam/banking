/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

/**
 *
 * @author ICIT
 */
public interface Bank {
    public int withdraw(int amount);
    public int deposit(int amount);
    public void showBalance();
    
}
