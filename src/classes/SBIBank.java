/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import interfaces.Bank;
import javax.swing.JOptionPane;

/**
 *
 * @author Administrator
 */
public class SBIBank implements Bank {
    int balance;
    

    public SBIBank(int balance) {
        this.balance = balance;
        System.out.println("Balance="+balance);
    }
    public int deposit(int amount){
        
        balance = balance+amount;
        
        
        return balance;
    }
    public int withdraw(int amount){
        balance = balance-amount;
        
               return balance; 
    }
    public void showBalance(){
        System.out.println("Balance="+balance);
    }
    
    
  

    
}
