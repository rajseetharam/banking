/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import model.Bank;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Administrator
 */
public class BusinessLogic {

    public void getBankDetails() {
        Configuration cnf = new Configuration();
        cnf.configure("hibernate.cfg.xml");

        StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder().applySettings(cnf.getProperties());

        SessionFactory sf = cnf.buildSessionFactory(ssrb.build());

        Session session = sf.openSession();

        String hql = "FROM Bank";

        List list = session.createQuery(hql).list();

        for (Iterator iterator = list.iterator(); iterator.hasNext();) {
            Bank b = (Bank) iterator.next();
            System.out.println(b);
            System.out.println(b.getBalance());
            System.out.println(b.getBankName());
            System.out.println(b.getAccountNumber());
            System.out.println(b.getAddedDate());
            System.out.println(b.getUpdatedDate());

        }
    }

    public void CreateRecord() {
        Configuration cnf = new Configuration();
        cnf.configure("hibernate.cfg.xml");

        StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder().applySettings(cnf.getProperties());

        SessionFactory sf = cnf.buildSessionFactory(ssrb.build());

        Session session = sf.openSession();

        Transaction t = session.beginTransaction();
        Bank b = new Bank(2, 2000, "hdfc", 1, new Date(), new Date());
        session.save(b);
        t.commit();

        System.out.println("Record is added");
        session.close();

    }
    
    public int deposit(int account_number, int amount) {
        
        Configuration cnf = new Configuration();
        cnf.configure("hibernate.cfg.xml");

        StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder().applySettings(cnf.getProperties());

        SessionFactory sf = cnf.buildSessionFactory(ssrb.build());

        Session session = sf.openSession();

        Transaction t = session.beginTransaction();
        
        String hql = "UPDATE Bank set balance = :bal WHERE id = :id";
        
        
        Query query = session.createQuery(hql);
        query.setParameter("bal", amount);
        query.setParameter("id", account_number);
        
        int query_result = query.executeUpdate();
        
        t.commit();
        
        return query_result;
    }
    public int closingAccount(int account_number){
      Configuration cnf = new Configuration();
        cnf.configure("hibernate.cfg.xml");

        StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder().applySettings(cnf.getProperties());

        SessionFactory sf = cnf.buildSessionFactory(ssrb.build());

        Session session = sf.openSession();

        Transaction t = session.beginTransaction();
        
        String hql = "UPDATE Bank set status = 0 WHERE id = :id";
        
        
        Query query = session.createQuery(hql);
       
        query.setParameter("id", account_number);
        
        int query_result = query.executeUpdate();
        
        t.commit();
        
        return query_result;
        
    }

}
